# RedBUX-Contracts
## Dependencies

* `truffle` installed globally

## Usage

* run `npm i` before all

* run `ganache-cli` or `truffle develop` for a local blockchain 

  *  note: adjust `truffle.js` accordingly

* run `truffle migrate` to deploy the contracts

* run `truffle test` to run the token-test-suite

