const suite = require('../node_modules/token-test-suite/lib/suite');
const RedBUX = artifacts.require('RedBUX');

contract('RedBUX', function (accounts) {
	let options = {
		// accounts to test with, accounts[0] being the contract owner
		accounts: accounts,

		// factory method to create new token contract
		create: async function () {
			return await RedBUX.new();
		},

		// factory callbacks to mint the tokens
		// use "transfer" instead of "mint" for non-mintable tokens
		transfer: async function (token, to, amount) {
			return await token.transfer(to, amount, { from: accounts[0] });
		},

		// optional:
		// also test the increaseApproval/decreaseApproval methods (not part of the ERC-20 standard)
		increaseDecreaseApproval: true,

		// token info to test
		name: 'RedBUX',
		symbol: 'BUX',
		decimals: 18,

		// initial state to test
		initialSupply: 20000000000e18,
		initialBalances: [
			[accounts[0], 20000000000e18]
		],
		initialAllowances: [
			[accounts[0], accounts[1], 0]
		]
	};

	suite(options);
});