pragma solidity ^0.4.20;

import "zeppelin-solidity/contracts/token/ERC20/StandardToken.sol";

contract RedBUX is StandardToken {

    string public name = "RedBUX";
    string public symbol = "BUX";
    uint8 public decimals = 18;
    uint256 public INITIAL_SUPPLY = 20000000000e18; //20b RedBUX Token with 18 Decimals

    function RedBUX() public { 
        totalSupply_ = INITIAL_SUPPLY;     
        balances[msg.sender] = INITIAL_SUPPLY;  //Contract owner gets whole supply
    }  

} 